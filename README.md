Getting started

# Fast start

## Clone 

```
git clone git@gitlab.myhost:infra/ansible.git
cd ansible
```

## Installing ansible through a "pip", the Python package manager (via Virtual Environment)
You may want to check what the latest stable version of ansible is before copying and running this command. Adjust the version in file `requirements.txt`

```
python3 -m venv venv
source venv/bin/activate
python3 -m pip install -r requirements.txt
```

Afterwards verify that ansible is installed by running `ansible --version`.

## Install prerequisites

`ansible-galaxy install -r requirements.yml`

`ansible all -m ping -v -i inventory.yaml`

`ansible-playbook -i inventory.yaml playbooks/docker.yaml`

`ansible-playbook -i inventory.yaml playbooks/runner.yaml`
